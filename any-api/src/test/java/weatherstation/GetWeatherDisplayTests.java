package weatherstation;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetWeatherDisplayTests {
	private static String StationName = "cnarenal";
	private static String Period;
		
	@Test
	public void GetWeatherDisplayTests_latestdata(){	
		//Arrange
		Period = "latestdata";
		String url = "https://api.oceandrivers.com/v1.0/getWeatherDisplay/%s/?period=%s";		
		String finalUrl = String.format(url, StationName, Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
				
	}
	@Test
	public void GetWeatherDisplayTests_latesthour(){	
		//Arrange
		Period = "latesthour";
		String url = "https://api.oceandrivers.com/v1.0/getWeatherDisplay/%s/?period=%s";		
		String finalUrl = String.format(url, StationName, Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
				
	}
	@Test
	public void GetWeatherDisplayTests_latestday(){		
		//Arrange
		Period = "latestday";
		String url = "https://api.oceandrivers.com/v1.0/getWeatherDisplay/%s/?period=%s";		
		String finalUrl = String.format(url, StationName, Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
				
	}	
	
	@Test
	public void GetWeatherDisplayTests_latestday_Negativetest(){		
		//Arrange
		Period = "";
		String url = "https://api.oceandrivers.com/v1.0/getWeatherDisplay/%s/?period=%s";		
		String finalUrl = String.format(url, "", Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 404);			
				
	}	
}

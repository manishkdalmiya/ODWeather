package weatherstation;


import org.testng.Assert;
import org.testng.annotations.Test;

import groovy.transform.stc.FromString;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetEasywindTests {
	private static String EasyWindId = "EW013";
	private static String Period;

	@Test
	public void getEasyWind_latestdata(){
		//Arrange
		Period = "latestdata";
		String url = "https://api.oceandrivers.com/v1.0/getEasyWind/%s/?period=%s";		
		String finalUrl = String.format(url, EasyWindId, Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);
		
		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);	
		Assert.assertEquals(response.jsonPath().get("ID"), EasyWindId, "Ids did not match");	
				
	}
	
	@Test
	public void getEasyWind_latestdata_NegativeTest(){
		//Arrange
		Period = "lastdate";
		String url = "https://api.oceandrivers.com/v1.0/getEasyWind/%s/?period=%s";		
		String finalUrl = String.format(url, EasyWindId, Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);
		
		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);				
		Assert.assertEquals(response.jsonPath().get("error"), "Operation not implemented on this meteostation");
	}
	
	@Test
	public void getEasyWind_latesthour(){		
		//Arrange
		Period = "latesthour";
		String url = "https://api.oceandrivers.com/v1.0/getEasyWind/%s/?period=%s";		
		String finalUrl = String.format(url, EasyWindId, Period);
				
		//Act
		Response response = RestAssured.get(finalUrl);
		
		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
				
	}
	@Test
	public void getEasyWind_latestday(){		
		//Arrange
		Period = "latestday";
		String url = "https://api.oceandrivers.com/v1.0/getEasyWind/%s/?period=%s";		
		String finalUrl = String.format(url, EasyWindId, Period);
				
		//Act
		Response response = RestAssured.get(finalUrl);
		
		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);		
				
	}
	
}

package weatherstation;


import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetAemetStationTests {
	private static String StationName = "aeropuertopalma";
	private static String Period;

	@Test
	public void getAemetStation_lastdata(){	
		//Arrange
		Period = "lastdata";
		String url = "https://api.oceandrivers.com/v1.0/getAemetStation/%s/%s/";		
		String finalUrl = String.format(url, StationName, Period);
			
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
	}
	@Test
	public void getAemetStation_lastdata_NegativeTest(){	
		//Arrange
		Period = "";
		String url = "https://api.oceandrivers.com/v1.0/getAemetStation/%s/%s/";		
		String finalUrl = String.format(url, StationName, Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 404);	
	}
	
	@Test
	public void getAemetStation_lastday(){		
		//Arrange
		Period = "lastday";
		String url = "https://api.oceandrivers.com/v1.0/getAemetStation/%s/%s/";		
		String finalUrl = String.format(url, StationName, Period);
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
				
	}

	
}

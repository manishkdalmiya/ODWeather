package weatherstation;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetForecastPointsTests {
	private static String yatchclubid = "cnarenal";
	
	@Test
	public void GetForecastPoints_en(){		
		//Arrange
		String locale = "en";
		String url = "https://api.oceandrivers.com/v1.0/getForecastPoints/%s/language/%s";	
		String finalUrl = String.format(url, yatchclubid, locale);
		
		//Act		
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
				
	}
	
	@Test
	public void GetForecastPoints_en_NegativeTest(){		
		//Arrange
		String locale = "";
		String url = "https://api.oceandrivers.com/v1.0/getForecastPoints/%s/language/%s";	
		String finalUrl = String.format(url, yatchclubid, locale);
		
		//Act		
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 404);			
				
	}
}

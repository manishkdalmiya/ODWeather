package weatherstation;


import org.testng.Assert;
import org.testng.annotations.Test;

import groovy.transform.stc.FromString;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Users {

	@Test
	public void getUser(){		
		//Arrange
		String url = "https://reqres.in/api/users?page=2";	
		
		//Act
		Response response = RestAssured.get(url);	
		
		//Assert
		Assert.assertEquals(200, response.getStatusCode(), "response Codes didn't match");
				
	}
	
	@Test
	public void postUser(){		
		//Arrange
		String url = "https://reqres.in/api/users";		
		String contentType = "application/json";
		String bodyData = 	"{\"name\":\"anonymous\",\"job\":\"leader\"}";
		
		//Act
		Response response = RestAssured
							.given()
							.contentType(contentType)
							.body(bodyData)
							.when()
							.post(url)
							.then()
							.extract()
							.response();					
			
		//Assert
		Assert.assertEquals(201, response.getStatusCode(), "response Codes didn't match");		
		Assert.assertEquals(response.jsonPath().get("name"), "anonymous", "Name posted doesn not match with the name retrieved!!");
				
	}
	
}

package weatherstation;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetForecastTimeSeriesTests {
	private static String lat = "39.49";
	private static String lon = "2.74";
	
	@Test
	public void GetForecastTimeSeries_lat_long(){		
		//Arrange
		String url = "https://api.oceandrivers.com/v1.0/getForecastTimeSeries/%s/%s/";	
		String finalUrl = String.format(url, lat, lon);
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 200);			
				
	}
	
	@Test
	public void GetForecastTimeSeries_lat_long_NegativeTest(){		
		//Arrange
		String url = "https://api.oceandrivers.com/v1.0/getForecastTimeSeries/%s/%s/";	
		String finalUrl = String.format(url, "", "");
		
		//Act
		Response response = RestAssured.get(finalUrl);

		//Assert
		Assert.assertEquals(response.getStatusCode(), 404);			
				
	}
}

# any-api 

### Assumptions
- maven is installed, otherwise follow instructions from https://maven.apache.org/ and install maven and set the build path
- An IDE such as eclipse is required


### Tools used
- maven for build
- TestNg as test framework


### Steps:-
#### Clone the git repository

#### Build the project to resolve all maven dependencies
- Right click on project and select Build Project

#### Run the TestNg project to execute all tests in package weatherstation
- right click on the package and run as TestNg

#### Locate Reports and open  the file in browser to see the run result
- Once the tests are done runnung, refresh the project to bring the output test-folder
- Locate index.html and open the file in browser to view the report in web view
